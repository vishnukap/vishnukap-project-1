import bagel.*;
import bagel.util.Point;

/**
 * Skeleton Code for SWEN20003 Project 1, Semester 2, 2021
 *
 * Please filling your name below
 * @author: Vishnudutt Kappagantula
 * @student_id: 1180554
 */
public class ShadowFlap extends AbstractGame {
    private Image Background;
    private Bird bird;
    private Pipe pipe;
    private int score = 0;
    private String gameState = "mainMenu", temp = "";

    /* Messages to display on screen */
    private final String startString = "PRESS SPACE TO START";
    private final String outOfBounds = "GAME OVER!";
    private final String finalScore = "Final Score: ";
    private final String winFont = "CONGRATULATIONS!";
    private final String scoreFont = "Score: ";

    private final Font font = new Font("res/slkscr.ttf", 48);
    private Point scorePoint = new Point(100, 100); /* Score position on screen */
    private final int secondLineGap = 75; /* 75 pixel gap between lines on screen */

    public ShadowFlap() {
        super(1024, 768, "Flappy Bird");
        this.Background = new Image("res/background.png");
        bird = new Bird();
        pipe = new Pipe();
    }

    /**
     * The entry point for the program.
     */
    public static void main(String[] args) {
        ShadowFlap game = new ShadowFlap();
        game.run();
    }

    /**
     * Performs a state update.
     * allows the game to exit when the escape key is pressed.
     */
    @Override
    public void update(Input input) {
        Background.draw(Window.getWidth() / 2.0, Window.getHeight() / 2.0);

        if(this.gameState == "started") {
            /* game has started */


            font.drawString(scoreFont + String.valueOf(this.score), scorePoint.x, scorePoint.y);

            bird.velocity -= bird.acceleration; /* update bird velocity */
            bird.velocity = Math.max(bird.velocity, bird.terminalVelocity); /* set fall velocity limit */
            bird.y -= bird.velocity; /* update bird position on screen */

            if(input.wasPressed(Keys.SPACE)) { bird.velocity = 6; }
            if(bird.y > Window.getHeight() || bird.y < 0) {
                /* bird went out of bound */
                gameState = "outOfBounds";
            };

            bird.Render(); /* render bird every frame */
            Point birdPos = new Point(bird.x, bird.y);
            temp = pipe.Render(bird.birdWingDown.getBoundingBoxAt(birdPos));
            if(temp == null) {
                /* bird did not go out of bounds or collide */
                temp = "";
            }
            else {
                /* bird went out of bounds or collided with pipe*/
                gameState = temp; /* update gamestate to gameOutOfBounds */
            }

            if(bird.x > pipe.pos) {
                /* successfully passed a pair of pipes */
                this.score = 1;
            }

            if(pipe.pos <= 0) {
                /* if centre of pipe hits left border, win condition activates */
                this.gameState = "win";
            }
        }
        else if (gameState == "outOfBounds") {
            Background.draw(Window.getWidth() / 2.0, Window.getHeight() / 2.0);
            font.drawString(outOfBounds,
                    (Window.getWidth() / 2.0) - (font.getWidth(outOfBounds) / 2.0),
                    Window.getHeight() / 2.0);
            font.drawString(finalScore + String.valueOf(this.score),
                    (Window.getWidth() / 2.0) - (font.getWidth(finalScore + String.valueOf(this.score)) / 2.0),
                    (Window.getHeight() / 2.0) + secondLineGap);
        }
        else if (gameState == "win") {
            Background.draw(Window.getWidth() / 2.0, Window.getHeight() / 2.0);
            font.drawString(winFont,
                    (Window.getWidth() / 2.0) - (font.getWidth(winFont) / 2.0),
                    Window.getHeight() / 2.0);
            font.drawString(finalScore + String.valueOf(this.score),
                    (Window.getWidth() / 2.0) - (font.getWidth(finalScore + String.valueOf(this.score)) / 2.0),
                    (Window.getHeight() / 2.0) + secondLineGap);
        }
        else {
            /* loading screen */
            if (input.wasPressed(Keys.SPACE)) {
                gameState = "started";
            }

            /* draw font exactly in the middle of the screen */
            font.drawString(startString, (Window.getWidth() / 2.0) - (font.getWidth(startString) / 2.0),
                    Window.getHeight() / 2.0);
        }

        if (input.wasPressed(Keys.ESCAPE)) {
            Window.close();
        }
    }

}
