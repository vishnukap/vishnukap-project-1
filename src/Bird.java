import bagel.Image;

public class Bird {
    /* positive means moving up, negative means moving down */
    public double velocity = 0;
    private static int frameCounterSinceStart = 1;

    private Image birdWingUp = new Image("res/birdWingUp.png");
    public Image birdWingDown = new Image("res/birdWingDown.png");

    public final double acceleration = 0.4;
    public final double terminalVelocity = -10;

    /* initial bird coords */
    public final double x = 200;
    public double y = 350;

//    public void Bird() {
//    }

    public void Render() {

        if(this.frameCounterSinceStart % 10 == 0) {
            this.birdWingUp.draw(this.x, this.y); /* repeat every 10 frames */
        }
        else {
            this.birdWingDown.draw(this.x, this.y);
        }

        this.frameCounterSinceStart++;
    }
}