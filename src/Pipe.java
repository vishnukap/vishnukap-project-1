import bagel.DrawOptions;
import bagel.Image;
import bagel.Window;
import bagel.util.Point;
import bagel.util.Rectangle;

public class Pipe {
    private final double velocity = -5;
    public double pos = Window.getWidth(); /* x-coord at spawn */
    private final int pixelGap = 168; /* gap between pipes */
    private Point centrePipe1, centrePipe2;

    /* the higher this value the higher the pipe gap spawns, vice versa */
    private final int initialY = 50; /* y-value of pipe at spawn */

    private Image pipeImage1 = new Image("res/pipe.png");
    private Image pipeImage2 = new Image("res/pipe.png");

    private Rectangle rectPipe1, rectPipe2;

//    public void Pipe() {
//
//    }

    public String Render(Rectangle bird) {
        String gameState; /* String to contain the state of the game */

        /* upside down pipe */
        this.pipeImage1.draw(this.pos, -this.initialY);
        centrePipe1 = new Point(this.pos, -this.initialY);

        /* generate hit-point rectangle for top pipe */
        rectPipe1 = this.pipeImage1.getBoundingBoxAt(centrePipe1);

        /* straight up pipe */
        DrawOptions options = new DrawOptions();
        this.pipeImage2.draw(this.pos, Window.getHeight() -this.initialY +this.pixelGap,
                options.setRotation(Math.PI));
        centrePipe2 = new Point(this.pos, Window.getHeight() -this.initialY +this.pixelGap);

        /* generate hit-point rectangle for bottom pipe */
        rectPipe2 = this.pipeImage2.getBoundingBoxAt(centrePipe2);

        if(rectPipe1.intersects(bird) || rectPipe2.intersects(bird)) {
            /* bird collides with pipes */
            gameState = "outOfBounds";
            return gameState;
        }

        this.pos += this.velocity; /* update pipe location */
        return null;
    }
}